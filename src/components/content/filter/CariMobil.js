import HeaderFilter from './HeaderFilter';
import '../../template/style.css';
import 'bootstrap/dist/css/bootstrap.css';
import { Container, Row, Col } from 'react-bootstrap';
import axios from 'axios';
import { useEffect, useState } from 'react';
import Swal from 'sweetalert2';


const CariMobil = () => {
  
  const [filteredList, setFilteredList] = useState([]);
  const [resultList, setResultList]     = useState([]);
  const [date, setDate]                 = useState("");
  const [time, setTime]                 = useState("");
  const [capacity, setCapacity]         = useState("");

  const listData =async () =>{
    
    try {
      const getData = await axios.get("https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json")
      console.log('data', getData.data)
      setFilteredList(getData.data)
      console.log('setFilter', setFilteredList(getData.data))
      
    } catch (error) {
      console.log('error', error)
    }
  }

  const handleSubmit = (e) =>{
    e.preventDefault()
    var getData    = listData();
    var resultFilter = getData.data;
    

    if (!resultFilter) {
      const result = filteredList.filter((car) =>  {
        if (date === "") {
          return car.capacity >= capacity;
        } 
        else if(date != null){
          return car.available === true && car.availableAt.slice(0, 10) >= date && car.capacity >= capacity;
        }else{
         return(
           Swal.fire({
            title:'Ooops ...',
            text :'No Data Availble',
            icon:'warning',
            confirmButtonColor:'#3085d6',
            cancelButtonColor:'#d33',
            confirmButtonText:'Back To List'
          }));
        }
      });
      setResultList(result)
      console.log('result', result)
      return  setResultList(result)

    }
    else{
      Swal.fire({
        title:'Ooops ...',
        text :'No Data Availble',
        icon:'warning',
        confirmButtonColor:'#3085d6',
        cancelButtonColor:'#d33',
        confirmButtonText:'Back To List'
      })
    }
  }

 const handleChangeDate =(e) =>{
  setDate(e.target.value)
 }
 const handleChangeTime =(e) =>{
  setTime(e.target.value)
  }
  const handleChangeCapacity=(e) =>{
    setCapacity(e.target.value)
  }

  useEffect(() => {
    listData();
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[date, time, capacity]);

  return (
    <div>

      <HeaderFilter />
        <section id="filter" className="filer">
          <div className="container" style={{paddingLeft:"3rem"}}>
              <div className="card bg-white shadow-lg rounded-3" 
              style={{marginTop:"-5rem", marginBottom:"5px", paddingTop:"3rem",paddingBottom:"3rem",alignItems:"center", paddingLeft: "2rem",paddingRight:"2rem"}}>
                  <Container>
                    <form action='/cars' onSubmit={handleSubmit}>
                      <Row className="justify-content-md-center">
                        <Col lg="3">
                          <label 
                              style={{fontFamily: 'Rubik',
                              fontStyle: "normal",
                              fontWeight: "300",
                              fontSize: "12px",
                              lineHeight: "18px",
                              color: "#3C3C3C"}}
                          >Tipe Driver</label>
                          <select className="form-select form-select-lg"  
                              style={{fontFamily: 'Helvetica',
                              fontStyle: "normal",
                              fontWeight: "300",
                              fontSize: "12px",
                              lineHeight: "18px",
                              color: "#3C3C3C"}} id="driver" name="driver">
                              <option disabled>Pilih Tipe Driver</option>
                              <option value="Dengan Sopir">Dengan Sopir</option>
                              <option value="Tanpa Sopir">Tanpa Sopir</option> 
                          </select>
                        </Col>
  
                        <Col lg="3">
                          <label
                              style={{fontFamily: 'Rubik',
                              fontStyle: "normal",
                              fontWeight: "300",
                              fontSize: "12px",
                              lineHeight: "18px",
                              color: "#3C3C3C"}}
                          >Tanggal</label>
                          <input 
                          className="form-control form-control-lg" 
                          type="date"
                          name="date" 
                          placeholder="Pilih Tanggal"
                          value={date}
                          onChange={handleChangeDate}
                          style={{fontFamily: 'Helvetica',
                          fontStyle: "normal",
                          fontWeight: "300",
                          fontSize: "12px",
                          lineHeight: "18px",
                          color: "#3C3C3C"}} />
                        </Col>
                        
                        <Col lg="2">
                          <label  
                              style={{fontFamily: 'Rubik',
                              fontStyle: "normal",
                              fontWeight: "300",
                              fontSize: "12px",
                              lineHeight: "18px",
                              color: "#3C3C3C"}}
                          >Waktu Jemput/Ambil </label>
                          <input 
                          type="time" 
                          className="form-control" 
                          name="time"
                          value={time}
                          onChange={handleChangeTime} />
                        </Col>
  
                        <Col lg="2">
                          <label
                              style={{fontFamily: 'Rubik',
                              fontStyle: "normal",
                              fontWeight: "300",
                              fontSize: "12px",
                              lineHeight: "18px",
                              color: "#3C3C3C"}}
                          >Jumlah Penumpang</label>
                          <input
                          className="form-control" 
                          type="number"
                          name="capacity" 
                          placeholder="Penumpang"
                          value={capacity}
                          onChange={handleChangeCapacity}
                          min="0"
                          style={{fontFamily: 'Helvetica',
                          fontStyle: "normal",
                          fontWeight: "300",
                          fontSize: "12px",
                          lineHeight: "18px",
                          color: "#3C3C3C"}} />
                        </Col>
  
                        <Col lg="2">
                          <label className="text-white mb-2">.</label>
                          <button type="submit" className="btn btn-success btn-lg" id="load-btn" 
                          style={{
                          marginTop:"1.5rem",
                          fontFamily: 'Helvetica',
                          fontStyle: "normal",
                          fontWeight: "200",
                          fontSize: "12px",
                          lineHeight: "18px",
                          backgroundColor:"#5CB85F",
                          color: "white"}}>Cari Mobil</button>
                        </Col>
  
                      </Row>
                    </form>
                  </Container>
              </div>
          </div>
  
        </section>
       
          <div className="hasilCari mt-5">
              <div className="container">
                  <div className="row">
                    {resultList.map((item) =>(
                      <div 
                      className='col-md-4'
                      style={{marginBottom: "2rem",
                            paddingTop: "3rem",
                            paddingLeft: "3rem"}} 
                      key={item.id}>
                            <div className="cars-container" id="cars-container">
      
                              <div className="card bg-white shadow-sm rounded-3">
                                <div className="card-body">
                                    <div className="image" style={{ marginBottom: "4rem"}}>
                                        <img src={item.image} alt={item.manufacture} 
                                        style={{ width:"100%", margin: "auto", height: "20rem"}} />
                                    </div>
      
                                    <div 
                                    className="mobilName"
                                    style={{ fontFamily: 'Helvetica',
                                    fontStyle: "normal",
                                    fontWeight: "400",
                                    fontSize: "14px",
                                    lineHeight: "20px",
                                    color: "#000000"}}>
                                        <p>{item.manufacture}</p>
                                    </div>
      
                                    <div className="harga">
                                        <p>{item.rentPerDay}</p>
                                    </div>
                                    <div className="description">
                                        <p>{item.description}</p>
                                    </div>
      
                                    <div className="spesifikasi">
                                        <div className="kapasitas mt-3 mb-2">
                                            <div className="row">
                                                <div className="col-lg-2 col-md-2">
                                                <img src="./images/fi_users.png" alt="kapasitas"/>
                                                </div>
                                                <div className="col-lg-8 col-md-8 text-left">
                                                    <p>{item.capacity}</p>
                                                </div>
                                            </div> 
                                        </div>
      
                                        <div className="setting mb-2">
                                            <div className="row">
                                                <div className="col-lg-2 col-md-2">
                                                <img src="./images/fi_settings.png" alt="kapasitas"/>
                                                </div>
                                                <div className="col-lg-8 col-md-8 text-left">
                                                    <p>{item.transmission}</p>
                                                </div>
                                            </div> 
                                        </div>
      
                                        <div className="year mb-2">
                                            <div className="row">
                                                <div className="col-lg-2 col-md-2">
                                                <img src="./images/fi_calendar.png" alt="kapasitas"/>
                                                </div>
                                                <div className="col-lg-8 col-md-8 text-left">
                                                    <p>{item.year}</p>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
                                    
                                    <div className="pilihMobil mb-2">
                                        <button className="btn btn-lg text-white" type="submit" style={{width:"100%",backgroundColor:"#5CB85F"}}>Pilih Mobil</button>
                                    </div>
      
                                </div>
                              </div>
      
                            </div>
                      </div>
                    
                    ))}
                  </div>
              </div>
          </div>
       
      
    </div>
  )
}

export default CariMobil