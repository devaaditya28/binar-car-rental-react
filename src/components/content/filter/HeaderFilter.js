import '../../template/style.css';
import logo from '../../images/logo.png';
import img_car from '../../images/img_car.png';
import { Link } from 'react-router-dom';

const HeaderFilter = () => {
  return (
    // <!--Project By : Muhamad Rizaludin-->
    // <!--Header-->
    <section id="main" className="main">
      {/* <!--Navbar--> */}
      <div className="page-wrap">
        <div className="nav-style">

          {/* <!--Mobile view--> */}
          <div className="mobile-view">
            <div className="mobile-view-header">
              <p><span >BCR</span></p>
              <i className="fa fa-times js-toggle justify-content-around"></i>
            </div>
            <div className="mobile-view-body"></div>
          </div>

          <div className="navbar navbar-expand-lg navbar-light bg-transparent" >
              <div className="container-fluid" >
                  <Link className="navbar-brand" to="/" >
                      <img src={logo} style={{ width:"100px", height:"34px" }} alt="logo" loading="lazy" />
                  </Link>
                  <div className="d-inline-block d-lg-none ml-md-0 ml-auto py-3"><i className="fas fa-bars js-toggle" style={{ fontSize:"25px", color:"black" }}></i></div>
                  <div className="d-none d-xl-block ms-auto">
                      <ul className="navbar-nav ms-auto js-clone-nav">
                          <li className="nav-item active">
                              <Link className='nav-link active ms-3' to="/#ourServis"><span>Our Services</span></Link>
                          </li>
                          <li className="nav-item ms-3">
                              <Link className="nav-link" to="/#WhyUs"><span>Why Us</span> </Link>
                          </li>
                          <li className="nav-item ms-3">
                              <Link className="nav-link" to="/#Testimonial"><span>Testimonial</span></Link>
                          </li>
                          <li className="nav-item ms-3">
                              <Link className="nav-link" to="/#FAQ"><span>FAQ</span></Link>
                          </li>
                          <li className="nav-item ms-3">
                              <Link className="btn btn-lg btn-success" to="/#register">Register</Link>
                          </li>
                      </ul>
                  </div>
              </div>
          </div>
          <div className="head-content">
            <div className="row g-0">
              <div className="col-xl-6 col-lg-6 align-self-center p-5">
                <h1 >Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)</h1>
                <p >Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk sewa mobil selama 24 jam.</p>
              </div>
              <div className="col-xl-6 col-lg-6 pt-5 ps-3" >
                <img src={img_car} style={{width:"100%",height: "100%"}} alt="imgCar" />
              </div>
            </div>
            
          </div> 
        </div> 
      </div>
    </section>
  )
}

export default HeaderFilter