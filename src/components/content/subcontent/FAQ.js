import '../../template/style.css';

const FAQ = () => {
  return (
    // <!--Content FAQ-->
    <section id="FAQ">
        <div className="FAQ mt-5 mb-5 ps-3 pe-3">
            <div className="container">
                <div className="row">
                    <div className="col-lg-5 col-md-5 col-xl-5 pb-5">
                        <h1>Frequently Asked Question</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                    </div>
                    <div className="col-lg-7 col-md-7 col-xl-7">
                        <div className="accordion" id="accordionFAQ">
                            <div className="accordion-item border mb-3">
                              <h2 className="accordion-header" id="h-1">
                                <button className="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#target-1" aria-expanded="true" aria-controls="target-1">
                                  <p>Apa saja syarat yang dibutuhkan?</p>  
                                </button>
                              </h2>
                              <div id="target-1" className="accordion-collapse collapse" aria-labelledby="h-1" data-bs-parent="#accordionFAQ">
                                <div className="accordion-body">
                                  <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod”</p>
                                </div>
                              </div>
                            </div>
                            <div className="accordion-item border mb-3">
                              <h2 className="accordion-header" id="h-2">
                                <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#target-2" aria-expanded="false" aria-controls="target-2">
                                  <p>Berapa hari minimal sewa mobil lepas kunci?</p>  
                                </button>
                              </h2>
                              <div id="target-2" className="accordion-collapse collapse" aria-labelledby="h-2" data-bs-parent="#accordionFAQ">
                                <div className="accordion-body">
                                  <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod”</p>
                                </div>
                              </div>
                            </div>
                            <div className="accordion-item border mb-3">
                              <h2 className="accordion-header" id="h-3">
                                <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#target-3" aria-expanded="false" aria-controls="target-3">
                                  <p>Berapa hari sebelumnya sabaiknya booking sewa mobil?</p>  
                                </button>
                              </h2>
                              <div id="target-3" className="accordion-collapse collapse" aria-labelledby="h-3" data-bs-parent="#accordionFAQ">
                                <div className="accordion-body">
                                  <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod”</p>
                                </div>
                              </div>
                            </div>
                            <div className="accordion-item border mb-3">
                                <h2 className="accordion-header" id="h-4">
                                  <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#target-4" aria-expanded="false" aria-controls="target-4">
                                    <p>Apakah Ada biaya antar-jemput?</p> 
                                  </button>
                                </h2>
                                <div id="target-4" className="accordion-collapse collapse" aria-labelledby="h-4" data-bs-parent="#accordionFAQ">
                                  <div className="accordion-body">
                                    <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                      sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                      sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                      sed do eiusmod”</p>
                                  </div>
                                </div>
                            </div>
                            <div className="accordion-item border mb-3">
                            <h2 className="accordion-header" id="h-5">
                                <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#target-5" aria-expanded="false" aria-controls="target-5">
                                  <p>Bagaimana jika terjadi kecelakaan</p>  
                                </button>
                            </h2>
                            <div id="target-5" className="accordion-collapse collapse" aria-labelledby="h-5" data-bs-parent="#accordionFAQ">
                                <div className="accordion-body">
                                <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                    sed do eiusmod”</p>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
  )
}

export default FAQ