
import ic_complite from '../../images/icon_complete.png';
import ic_price from '../../images/icon_price.png';
import ic_clock from '../../images/icon_24hrs.png';
import ic_profesi from '../../images/icon_professional.png';
import '../../template/style.css';

const WhyUs = () => {
  return (
    // <!--Content WhyUs-->
    <section id="WhyUs">
      <div className="whyus ps-3 pe-3">
          <div className="container mt-5 mb-5">
              <h1>Why Us?</h1>
              <span >Mengapa harus pilih Binar Car Rental?</span>
          </div>
          <div className="container mb-5">
              <div className="row">
                  <div className="col-lg-3 pb-3">
                      <div className="card bg-white shadow-sm">
                          <img src={ic_complite} alt="ic-complete" />
                          <strong className="ps-4 pt-3">Mobil Lengakap</strong>
                          <div className="card-body">
                              <p>Tersedia banyak pilihan mobil, kondisi masih baru, bersih, berkualitas dan terawat</p>
                          </div>
                      </div>
                  </div>
                  <div className="col-lg-3 pb-3">
                      <div className="card bg-white shadow-sm">
                          <img src={ic_price} alt="ic-price" />
                          <strong className="ps-4 pt-3">Harga Murah</strong>
                          <div className="card-body">
                              <p>Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain</p>
                          </div>
                      </div>
                  </div>
                  <div className="col-lg-3 pb-3">
                      <div className="card bg-white shadow-sm">
                          <img src={ic_clock} alt="icon-24hrs" />
                          <strong className="ps-4 pt-3">Layanan 24 Jam</strong>
                          <div className="card-body">
                              <p>Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu</p>
                          </div>
                      </div>
                  </div>
                  <div className="col-lg-3 pb-3">
                      <div className="card bg-white shadow-sm">
                         <img src={ic_profesi} alt="ic-profesional" />
                          <strong className="ps-4 pt-3">Sopir Profesional</strong>
                          <div className="card-body">
                              <p>Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
                          </div>
                      </div>
                  </div>    
              </div>
          </div>
      </div>
    </section>
  )
}

export default WhyUs