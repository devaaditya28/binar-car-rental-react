import img_servis from '../../images/img_service.png'
import ic_ceklis from '../../images/ic-ceklis.png'
import '../../template/style.css'

const OurServices = () => {
  return (
    // <!--Content Ourservices-->
    <section id="OurServices">
      <div className="Ourservices mt-5 mb-5 ps-3 pe-3">
          <div className="container">
              <div className="row justify-content-around">
                  <div className="col-md-5 col-lg-5 col-xl-5">
                      <img src={img_servis} style={{width:"100%", height:"100%"}} alt="services" />
                  </div>
                  <div className="col-md-5 col-lg-5 col-xl-5">
                      <h1 className="pt-5">Best Car Rental for any kind of trip in (Lokasimu)!</h1>
                      <p className=" pt-5 pb-3">Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih 
                          murah dibandingkan yang lain, kondisi mobil baru, serta kualitas 
                          pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.
                      </p>
                      <p> <img src={ic_ceklis} alt="ic-ceklis" style={{width: "15px", height: "15px"}} /> Sewa Mobil Lepas Kunci di Bali 24 Jam</p>
                      <p> <img src={ic_ceklis} alt="ic-ceklis" style={{width: "15px", height: "15px"}} /> Sewa Mobil Jangka Panjang Bulanan</p>
                      <p> <img src={ic_ceklis} alt="ic-ceklis" style={{width: "15px", height: "15px"}} /> Gratis Antar - Jemput Mobil di Bandara</p>
                      <p> <img src={ic_ceklis} alt="ic-ceklis" style={{width: "15px", height: "15px"}} /> Layanan Airport Transfer / Drop In Out</p>
                  </div>
              </div>
          </div>
      </div>
    </section>
  )
}

export default OurServices