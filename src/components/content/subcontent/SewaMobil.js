
import { Link } from 'react-router-dom';
import '../../template/style.css';

const SewaMobil = () => {
  return (
    // <!--Content Sewa Mobil-->
     <section id="sewamobil">
      <div className="sewamobil ps-3 pe-3">
          <div className="container">
              <div className="jumbotron rounded-3 pb-5 pt-5">
                  <div className="container">
                      <div className="row">
                          <div className="col-lg-12 ps-3" style={{textAlign:"center"}}>
                              <h1 >Sewa Mobil di (Lokasimu) Sekarang</h1>
                                  <p >Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                                      sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                                  </p>
                              <Link className="btn btn-success btn-lg mb-5 mt-5 " to="/cars" role="button">Mulai Sewa Mobil</Link>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </section>
  )
}

export default SewaMobil