import ic_fb from '../images/icon_facebook.png';
import ic_ig from '../images/icon_instagram.png';
import ic_mail from '../images/icon_mail.png';
import ic_twit from '../images/icon_twitter.png';
import ic_twich from '../images/icon_twitch.png';
import logo from '../images/logo.png';
import { Link } from 'react-router-dom';
import './style.css';

const Footer = () => {
  return (
    // <!--Footer-->
    <footer className="footer bg-white">
        <div className="footer_top ps-3 pe-3 pb-3">
            <div className="container"> 
                <div className="row">
                    <div className="col-xl-3 col-md-6 col-lg-3">
                        <div className="footer-widget">
                            <p> Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000 </p>
                            <p> binarcarrental@gmail.com </p>
                            <p> 081-233-334-808 </p>
                        </div>
                    </div>
                    <div className="col-xl-3 col-md-6 col-lg-3">
                        <div className="footer_widget">
                            <Link to="/#OurServices" className="link-dark"><p>Our Services</p></Link>
                            <Link to="/#WhyUs" className="link-dark"><p>Why Us</p></Link>
                            <Link to="/#Testimonial" className="link-dark"><p>Testimonial</p></Link>
                            <Link to="/#FAQ" className="link-dark"><p>FAQ</p></Link>
                        </div>
                    </div>
                    <div className="col-xl-3 col-md-6 col-lg-3">
                        <div className="footer_widget">
                            <p className="pb-2">Connect with us</p>
                            <a href="#fb"><img src={ic_fb} alt="icon_facebook" /></a>
                            <a href="#ig"><img src={ic_ig} alt="icon_instagram" /></a>
                            <a href="#twt"><img src={ic_twit} alt="icon_twitter" /></a>
                            <a href="#mail"><img src={ic_mail} alt="icon_mail" /></a>
                            <a href="tw"><img src={ic_twich} alt="icon_twitch" /></a>
                        </div>
                    </div>
                    <div className="col-xl-3 col-md-6 col-lg-3 pt-3 mb-3">
                        <div className="footer_widget">
                            <p className="pb-2">Copyright Binar 2022</p>
                            <a href="#main"><img src={logo} alt="logo" /></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
  )
}

export default Footer