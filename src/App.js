import './App.css';
import Footer from './components/template/Footer';
import Home from './components/content/Home';
import CariMobil from './components/content/filter/CariMobil';
import {BrowserRouter, Routes, Route} from 'react-router-dom';

// import OurServices from './components/content/subcontent/OurServices';

function App() {
  return (
    <BrowserRouter>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="cars" element={<CariMobil />} />
          {/* <Route path="ourServis" element={<OurServices />} /> */}
        </Routes>
      <Footer />
    </BrowserRouter>
  );
}

export default App;
